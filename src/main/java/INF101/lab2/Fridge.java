package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

 


public class Fridge implements IFridge  
{

// feltvariabler 
    ArrayList<FridgeItem> fridgeItems;
    int max;
    

// Constructor 

    public Fridge () {
        fridgeItems= new ArrayList<>();
        max= 20;
        
    } 

// Methods
    @Override
    public int nItemsInFridge() {
        
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return max;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        
        int lengthOfArray = fridgeItems.size();
        if (lengthOfArray<max) {
            fridgeItems.add(item); 
                        return true;
        }
          else {
                    return false;
                }
             }

                    
    
    @Override
    public void takeOut(FridgeItem item) {

        boolean removedItem = fridgeItems.remove(item);
        if(!removedItem){ 
            throw new NoSuchElementException("Cannot find item in fridge");
    

        }


        }

    @Override
    public void emptyFridge() {   
        fridgeItems.clear();

        }
    

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();


        

        for(FridgeItem item : fridgeItems){

            if (item.hasExpired()){
                expiredItems.add(item);

            }
        
        

        }
        fridgeItems.removeAll(expiredItems);
                 return expiredItems;
    }
 
}